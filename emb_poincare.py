# -*- coding: utf-8 -*-
"""
Created on Fri Aug 31 09:20:20 2018

@author: simon
"""

from graph import from_networkx_graph_to_igraph


import numpy as np
import gensim.models.poincare

def get_poincare_embedding(networkx_graph, size=2, negative=2):
    igraph = from_networkx_graph_to_igraph(networkx_graph=networkx_graph)

    # Poincaré's model parameters

    # Train data as a list of edges 
    # that fulfill (if (a,b) in edges => (b,a) in edges)
    train_data = igraph.edges()

    # size (int, optional) 
    # – Number of dimensions of the trained model.

    # alpha (float, optional) – Learning rate for training.

    # negative (int, optional) 
    # – Number of negative samples to use.

    # workers (int, optional) 
    # – Number of threads to use for training the model.

    # epsilon (float, optional) 
    # – constantant used for clipping embeddings below a norm of one.

    # regularization_coeff (float, optional) 
    # – Coefficient used for l2-regularization while training (0 effectively disables regularization).

    # burn_in (int, optional) 
    # – Number of epochs to use for burn-in initialization (0 means no burn-in).

    # burn_in_alpha (float, optional) 
    # – Learning rate for burn-in initialization, ignored if burn_in is 0.

    # init_range (2-tuple (float, float)) 
    # – Range within which the vectors are randomly initialized.

    # seed (int, optional) – Seed for random to ensure reproducibility.

    model_poincare = gensim.models.poincare.PoincareModel(train_data,
                                                          size=size,
                                                          negative=negative,
                                                          seed=12345,
                                                          alpha=0.1,
                                                          burn_in=10,
                                                          burn_in_alpha=0.01)

    '''
    batch_size (int, optional) 
        – Number of examples to train on in a single batch.
    
    epochs (int)
        – Number of iterations (epochs) over the corpus.
    
    print_every (int, optional) 
        – Prints progress and average loss after every print_every batches.
    
    check_gradients_every (int or None, optional)
        – Compares computed gradients and autograd gradients after every check_gradients_every batches. Useful for debugging, doesn’t compare by default.
    '''
    model_poincare.train(epochs=50, batch_size=10, print_every=2)
    poincare_node_mat = model_poincare.kv.syn0
    return poincare_node_mat, model_poincare


def graph_reconstruction_from_poincare_emb(poincare_model, networkx_graph_or_degree_lst, avg_degree):
    """
    Uses K-nearest algorithm to find the neighbors for every node.
    The degree of every node of the original graph is mandatory.
    """
    
    if isinstance(networkx_graph_or_degree_lst, list):
        degree_lst = networkx_graph_or_degree_lst
    elif isinstance(networkx_graph_or_degree_lst, nx.Graph):
        degree_lst = np.array(networkx_graph_or_degree_lst.degree())[:,1].tolist()
    
    node_ids = list(poincare_model.kv.vocab.keys())
    
    # For every node
    
    for v in node_ids:
        v_i = int(v)
        
        v_degree = degree_lst[v_i]
        # k-nearest nodes (where k = degree)
        poincare_model.kv.most_similar(v, topn=v_degree)


