# -*- coding: utf-8 -*-
"""
Created on Fri Aug 31 09:23:35 2018

@author: simon
"""

from preprocessing import load_data, load_poincare_emb
from graph import avg_degree

import numpy as np


# Graph data
data = load_data()
karate_mat = data['karate_mat']
karate_graph = data['karate_graph']
catalog_mat = data['catalog_mat']
catalog_graph = data['catalog_graph']
fb_mat = data['fb_mat']
fb_graph = data['fb_graph']
karate_avg_degree = avg_degree(karate_graph)
fb_avg_degree = avg_degree(fb_graph)

# Embedded data
poincare_data = load_poincare_emb()
karate_poinc = poincare_data['std_karate_poincare']
fb_poinc = poincare_data['std_fb_poincare']


import emb_poincare

import constant
import tools
import gensim.models.poincare as pc
from gensim.models.poincare import LinkPredictionEvaluation

# Link Prediction Task


# TODO let MAP and RANK run over different epochs

fb_training_pre_fname = constant.data_dir + 'fb_training_'
fb_testing_pre_fname = constant.data_dir + 'fb_testing_'
karate_training_pre_fname = constant.data_dir + 'karate_training_'
karate_testing_pre_fname = constant.data_dir + 'karate_testing_'


import prettytable


def eval_poincare(graph, emb_dims=range(2,11), files=range(1,11)):
    pt = prettytable.PrettyTable()
    
    pt.add_column('emb_dim', list(emb_dims))
    
    drank = []
    dMAP = []
    
    for emb_dim in emb_dims:
        # Over 10 different training-/ testing-distributions
        ranks = []
        MAPs = []
        for i in files:
            _, poinc = emb_poincare.get_poincare_embedding(graph, size=emb_dim, negative=2)
            res = LinkPredictionEvaluation(karate_training_pre_fname + tools.int2str(i), 
                                          karate_testing_pre_fname + tools.int2str(i), 
                                          poinc.kv).evaluate()
            
            rank = res['mean_rank']
            MAP = res['MAP']
            
            ranks.append(rank)
            MAPs.append(MAP)
            
        
        avg_rank = np.mean(ranks)
        avg_MAP = np.mean(MAPs)
        
        drank.append(avg_rank)
        dMAP.append(avg_MAP)
        #print('avg_rank', avg_rank)    
        #print('avg_MAP', avg_MAP)
    pt.add_column('rank', drank)
    pt.add_column('MAP', dMAP)
    print(pt)
    print(emb_dims)
    print(drank)
    print(dMAP)

emb_dims = list(range(2,11)) + list(range(15,55,5))
#eval_poincare(karate_graph, emb_dims=emb_dims)

#eval_poincare(fb_graph, emb_dims=emb_dims)

#eval_poincare(fb_graph, emb_dims=emb_dims, files=[1])




import emb_node2vec

def eval_node2vec(graph, emb_dims=list(range(2,11)) + list(range(15,55,5)), files=range(1,11)):
    pt = prettytable.PrettyTable()
        
    pt.add_column('emb_dim', list(emb_dims))
    
    drank = []
    dMAP = []
    
    for emb_dim in emb_dims:
        # Over 10 different training-/ testing-distributions
        ranks = []
        MAPs = []
        for i in files:
            rank, MAP = emb_node2vec.get_node2vec_rank_and_map(graph, 
                                                    karate_training_pre_fname + tools.int2str(i),
                                                    karate_testing_pre_fname + tools.int2str(i),
                                                    node2vec_fun=lambda graph : 
                                                        emb_node2vec.get_node2vec_embedding(graph, dimensions=2))
            print(rank, MAP)
            ranks.append(rank)
            MAPs.append(MAP)
        avg_rank = np.mean(ranks)
        avg_MAP = np.mean(MAPs)
        
        drank.append(avg_rank)
        dMAP.append(avg_MAP)
        #print('avg_rank', avg_rank)    
        #print('avg_MAP', avg_MAP)
    pt.add_column('rank', drank)
    pt.add_column('MAP', dMAP)
    print(pt)
    print(emb_dims)
    print(drank)
    print(dMAP)


#eval_node2vec(fb_graph, emb_dims=emb_dims, files=[1])

from matplotlib import pyplot as plt
'''
EMBEDDING DIMENSION ITERATIONS
'''

n = len(emb_dims)

print('Average rank comparison on karate graph between poincare and node2vec embedding')

# Best dim=40
karate_ranks_poinc = [4.614754098360655, 4.560655737704919, 4.60983606557377, 
               4.634426229508197, 4.631147540983607, 4.390163934426229, 
               4.495081967213114, 4.486885245901639, 4.4081967213114766, 
               4.704918032786885, 4.677049180327868, 4.383606557377049, 
               4.565573770491804, 4.657377049180328, 4.7606557377049175, 
               4.545901639344263, 4.521311475409837]

_rep = lambda l, decimals=1 : str(np.round(np.mean(l),decimals=decimals))

# Best dim=25
karate_ranks_node2vec = [114.454436880615, 111.7071927387237, 113.24651060479762, 
                         114.21448643141613, 111.74504723534315, 111.64573816800961, 
                         113.98829350745578, 112.87318931184589, 113.3616351764379, 
                         117.11140918773735, 114.39343197184402, 121.24306474020561, 
                         111.28242521070668, 115.8197184403075, 112.85608594980087, 
                         114.77685607113088, 112.92739094192832]

plt.plot(emb_dims, karate_ranks_poinc, 'b.-', label="poincare (avg={})".format(_rep(karate_ranks_poinc)))
plt.plot(emb_dims, karate_ranks_node2vec, 'r.-', label="node2vec (avg={})".format(_rep(karate_ranks_node2vec)))
plt.xlabel('embedding dimensions')
plt.ylabel('rank')
plt.legend(loc='best')

plt.savefig('./results/karate_rank.pdf', dpi=400)
plt.show()


print('Average MAP comparison on karate graph between poincare and node2vec embedding')

# Best dim=40
karate_map_poinc = [0.4157970666453479, 0.4193780067105794, 0.4133167522222724, 
                    0.4134022587817925, 0.4134330680044135, 0.4229828329314502, 
                    0.42750552097240824, 0.42796340997596294, 0.42315093533109777, 
                    0.4168474408404341, 0.4207695187649027, 0.4392502427267203, 
                    0.4228106152409731, 0.4155346073466476, 0.40803361399389193, 
                    0.42218062778076393, 0.42600721335810227]

# Best dim=15
karate_map_node2vec = [0.24903819764567287, 0.26159492867720335, 0.250579013010813, 
                    0.2501425006609467, 0.258350781771528, 0.26098140112446744, 
                    0.2506314424533906, 0.2620819827725124, 0.25304180138076904, 
                    0.2435088463116312, 0.24598984442741081, 0.24992544179355658, 
                    0.25849568054754923, 0.2550087694750697, 0.25051256437961833, 
                    0.2599000153646748, 0.2594440451815559]

plt.plot(emb_dims, karate_map_poinc, 'b.-', label="poincare (avg={})".format(_rep(karate_map_poinc, 3)))
plt.plot(emb_dims, karate_map_node2vec, 'r.-', label="node2vec (avg={})".format(_rep(karate_map_node2vec, 3)))
plt.xlabel('embedding dimensions')
plt.ylabel('MAP')
plt.legend(loc='best')

plt.savefig('./results/karate_map.pdf', dpi=400)
plt.show()



print('Average rank comparison on fb graph between poincare and node2vec embedding')
# Best dim=25
fb_ranks_poinc = [6.56146814, 6.54383506, 6.54226717, 6.57067643, 6.56598531,
       6.35922381, 6.41116032, 6.48226088, 6.35250072, 6.64200717,
       6.59846746, 6.31982153, 6.53508201, 6.58318248, 6.69559488,
       6.53382166, 6.47231667]

_rep = lambda l, decimals=1 : str(np.round(np.mean(l),decimals=decimals))

# Best dim=30
fb_ranks_node2vec = [132.02079337, 132.15907513, 133.18154455, 135.38104321,
       135.42515131, 134.32910144, 134.12510223, 137.21728704,
       133.42606664, 133.73831158, 140.00363477, 135.5802886 ,
       133.55336312, 136.60423584, 135.58298168, 132.43334832,
       133.71286588]

plt.plot(emb_dims, fb_ranks_poinc, 'b.-', label="poincare (avg={})".format(_rep(fb_ranks_poinc)))
plt.plot(emb_dims, fb_ranks_node2vec, 'r.-', label="node2vec (avg={})".format(_rep(fb_ranks_node2vec)))
plt.xlabel('embedding dimensions')
plt.ylabel('rank')
plt.legend(loc='best')

plt.savefig('./results/fb_rank.pdf', dpi=400)
plt.show()


print('Average MAP comparison on fb graph between poincare and node2vec embedding')

# Best dim=25
fb_map_poinc = [0.39091926, 0.34620678, 0.33399368, 0.33278979, 0.35319757,
       0.41658556, 0.33936233, 0.40384449, 0.41803472, 0.36033329,
       0.36004257, 0.34413295, 0.33334587, 0.34273735, 0.37263166,
       0.35208641, 0.42542986]

# Best dim=30
fb_map_node2vec = [0.21582479, 0.18904785, 0.23151188, 0.24651083, 0.19839933,
       0.16714427, 0.21936053, 0.26098136, 0.15557543, 0.2542735 ,
       0.15661839, 0.15624885, 0.24811534, 0.23075739, 0.22492526,
       0.16854133, 0.21309511]

plt.plot(emb_dims, fb_map_poinc, 'b.-', label="poincare (avg={})".format(_rep(fb_map_poinc, 3)))
plt.plot(emb_dims, fb_map_node2vec, 'r.-', label="node2vec (avg={})".format(_rep(fb_map_node2vec, 3)))
plt.xlabel('embedding dimensions')
plt.ylabel('MAP')
plt.legend(loc='best')

plt.savefig('./results/fb_map.pdf', dpi=400)
plt.show()



import visualization
visualization.plot_poincare_model(karate_poinc, pdf_fname='results/std_karate_poincare.pdf',
                                  fontsize = 12,
                                  figsize = 12,
                                  markersize = 10,
                                  linewidth = 0.01)

visualization.plot_poincare_model(fb_poinc, pdf_fname='results/std_fb_poincare.pdf',
                                  fontsize = 2,
                                  figsize = 5,
                                  markersize = 10,
                                  linewidth = 0.01)
