# -*- coding: utf-8 -*-
"""
Created on Wed Aug 29 10:18:16 2018

@author: simon
"""


def macro_f1(mat_in, mat_out):
    return f1_score(mat_in, mat_out, average='macro')
