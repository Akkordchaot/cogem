# -*- coding: utf-8 -*-
"""
Created on Fri Aug 31 09:22:59 2018

@author: simon
"""

import tools
import constant
from graph import get_adj_mat
from emb_poincare import get_poincare_embedding


import os
import numpy as np
import gensim
import networkx as nx


def get_shuffeled_training_testing(edge_list, train_ratio=0.8):
    edges = edge_list.copy()
    training, testing = tools.split_array(edges, ratio=train_ratio)
    
    return training, testing


def write_ml_training_testing(graph, fname_prefix, train_ratio=0.8, n_times=10):
    """
    Writes n_times a shuffeled and split version of the edgelist
    of the given graph. This edgelist is split according to the
    train_ratio into a training and testing set of edges.
    Resulting in n_times files of the format:
        fname_prefix + "_training_01"
        fname_prefix + "_testing_01"
        ...
        fname_prefix + "_<ntimes>"
    """
    all_edges = list(graph.edges)

    for i in range(1, n_times + 1):
        
        training_fname = fname_prefix + '_training_' + ('%d'%(i)).zfill(2)
        testing_fname = fname_prefix + '_testing_' + ('%d'%(i)).zfill(2)
        training, testing = get_shuffeled_training_testing(all_edges, train_ratio=train_ratio)
       
        # Write training edgelist to file
        nx.write_edgelist(nx.Graph(training), training_fname, delimiter='\t', data=False)
        nx.write_edgelist(nx.Graph(testing), testing_fname, delimiter='\t', data=False)


def load_data():
    """
    :return:    data        A container object containing 
                            all networkx graphs and their adjacency matrices
    """
    
    if tools.all_files_in_data_dir(['catalog_graph', 'catalog_mat.pkl', 
                                    'karate_graph', 'karate_mat.pkl',
                                    'fb_graph', 'fb_mat.pkl']):
        print('Quickly load persisted graph data.')
        catalog_graph = nx.read_gpickle(constant.data_dir + 'catalog_graph')
        catalog_mat = tools.load('catalog_mat')
        karate_graph = nx.read_gpickle(constant.data_dir + 'karate_graph')
        karate_mat = tools.load('karate_mat')
        fb_graph = nx.read_gpickle(constant.data_dir + 'fb_graph')
        fb_mat = tools.load('fb_mat')

    else:
        print('construct networkx graph data and store it as gpickle.')
        
        karate_mat = get_adj_mat(filename=constant.zachary_karate_club_filepath)
        karate_graph = nx.from_numpy_matrix(karate_mat)    
        # Store shuffeled ML data Training (80%) and Testing (20%)
        write_ml_training_testing(karate_graph, constant.data_dir + 'karate', train_ratio=0.8, n_times=10)
        nx.write_gpickle(karate_graph, constant.data_dir + 'karate_graph')
        tools.save('karate_mat', karate_mat)
                
        catalog_mat = get_adj_mat(filename=constant.blogcatalog_filepath)
        catalog_graph = nx.from_numpy_matrix(catalog_mat)
        nx.write_gpickle(catalog_graph, constant.data_dir + 'catalog_graph')
        tools.save('catalog_mat', catalog_mat)
        
        fb_edges = get_adj_mat(filename=constant.fb_filepath)
        fb_graph = nx.from_edgelist(fb_edges)
        write_ml_training_testing(fb_graph, constant.data_dir + 'fb', train_ratio=0.8, n_times=10)
        fb_mat = np.array(nx.adjacency_matrix(fb_graph).todense())
        nx.write_gpickle(fb_graph, constant.data_dir + 'fb_graph')
        tools.save('fb_mat', fb_mat)
            
    data = {'karate_mat': karate_mat,
            'karate_graph': karate_graph,
            'catalog_mat': catalog_mat,
            'catalog_graph': catalog_graph,
            'fb_mat' : fb_mat,
            'fb_graph' : fb_graph}

    return data


def get_karate_poincare_emb():
    data = load_data()
    karate_graph = data['karate_graph']
    if not os.path.exists(constant.data_dir + 'std_karate_poincare'):
        mat, p = get_poincare_embedding(karate_graph, size=2, negative=2)
        p.save('std_karate_poincare')
    else:
        p = gensim.utils.SaveLoad.load(constant.data_dir + 'std_karate_poincare')
     
    return p
    

def load_poincare_emb():
    poinc_data = dict()
    
    data = load_data()
    karate_graph = data['karate_graph']
    fb_graph = data['fb_graph']
    
    if not tools.all_files_in_data_dir(['std_karate_poincare', 'std_fb_poincare']):
        karate_mat, std_karate_poincare = get_poincare_embedding(karate_graph, size=2, negative=2)
        std_karate_poincare.save(constant.data_dir + 'std_karate_poincare')
        
        fb_mat, std_fb_poincare = get_poincare_embedding(fb_graph, size=2, negative=2)
        std_fb_poincare.save(constant.data_dir + 'std_fb_poincare')
        
    else:
        std_karate_poincare = gensim.utils.SaveLoad.load(constant.data_dir + 'std_karate_poincare')
        std_fb_poincare = gensim.utils.SaveLoad.load(constant.data_dir + 'std_fb_poincare')
    poinc_data['std_karate_poincare'] = std_karate_poincare
    poinc_data['std_fb_poincare'] = std_fb_poincare
    return poinc_data

if __name__ == '__main__':
    
    data = load_data()
    karate_mat = data['karate_mat']
    karate_graph = data['karate_graph']
    catalog_mat = data['catalog_mat']
    catalog_graph = data['catalog_graph']
    fb_mat = data['fb_mat']
    fb_graph = data['fb_graph']
    
    poincare_data = load_poincare_emb()
    karate_poinc = poincare_data['std_karate_poincare']
    fb_poinc = poincare_data['std_fb_poincare']
    