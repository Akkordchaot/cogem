# -*- coding: utf-8 -*-
"""
Created on Fri Aug 31 09:20:54 2018

@author: simon
"""



import numpy as np
from node2vec import Node2Vec # Random walk embedding with node2vec
import networkx as nx

'''

Node2Vec constructor:

graph: The first positional argument has to be a networkx graph. Node names must be all intexgers or all strings. On the output model they will always be strings.
dimensions: Embedding dimensions (default: 128)
walk_length: Number of nodes in each walk (default: 80)
num_walks: Number of walks per node (default: 10)
p: Return hyper parameter (default: 1)
q: Inout parameter (default: 1)
weight_key: On weighted graphs, this is the key for the weight attribute (default: 'weight')
workers: Number of workers for parallel execution (default: 1)
sampling_strategy: Node specific sampling strategies, supports setting node specific 'q', 'p', 'num_walks' and 'walk_length'. Use these keys exactly. If not set, will use the global ones which were passed on the object initialization`
Node2Vec.fit method: Accepts any key word argument acceptable by gensim.Word2Vec
'''


def get_node2vec_embedding(
        digraph,
        # (bfs- or dfs-strategy (parametrized by p and q)
        p=1,
        q=1,
        # Depth of the random walk
        walk_length=80,  # Default 80
        # Invariance of initial node
        num_walks=10,  # Default 10
        # node2vec strategy object
        sampling_strategy=None,
        # Embedding dimensionality
        dimensions=30,  # Default 128
        # Paralellism
        workers=1,
        window=10,
        min_count=1,
        batch_words=4):
    # Model
    node2vec = Node2Vec(digraph,
                        dimensions=dimensions,
                        walk_length=walk_length,
                        workers=workers,
                        sampling_strategy=sampling_strategy)

    # Embedding
    model = node2vec.fit(window=window, min_count=min_count, batch_words=batch_words)

    # Look for most similar nodes
    # model.wv.most_similar('2')

    EMBEDDING_FILENAME = 'node2vec_embd_karate'
    EMBEDDING_MODEL_FILENAME = 'node2vec_model_karate'

    # Save embeddings for later use
    # model.wv.save_word2vec_format(EMBEDDING_FILENAME)

    # Save model for later use
    # model.save(EMBEDDING_MODEL_FILENAME)

    return model.wv.vectors, model


def evaluate_mean_rank_and_map(node2vec_model, fname_training, fname_testing):
    known_edges = list(nx.read_edgelist(fname_training))



def get_node2vec_rank_and_map(graph, fname_training, fname_testing,
                              node2vec_fun=lambda graph:get_node2vec_embedding(graph)):
        '''
        graph = karate_graph
        
        fname_training = 'data/karate_training_01'
        fname_testing = 'data/karate_testing_01'
        '''
        unknown_graph = nx.read_edgelist(fname_testing)
        unknown_edges = list(unknown_graph.edges)
        
        known_graph = nx.read_edgelist(fname_training)
        known_edges = list(known_graph.edges)
        _, model = node2vec_fun(known_graph)
        
        
        # Edges of complete graph. 
        # Undirected edges are assumed. 
        complete_edges = []
        nodes = list(graph.nodes)
        for i, node_source in enumerate(nodes[:-1]):
            for node_target in nodes[i+1:]:
                node_source = str(node_source)
                node_target = str(node_target)
                if node_source in model.wv.vocab and node_target in model.wv.vocab:
                    complete_edges.append((node_source, node_target))
        
                
        # Edges to add to the original graph in order to obtain a complete graph
        negative_edges = [negative_edge 
                          for negative_edge in complete_edges 
                          if (negative_edge not in known_edges 
                              and negative_edge not in unknown_edges)]
        negative_dists = np.array([model.wv.distance(*negative_edge) for negative_edge in negative_edges])
        ranks = []
        for unknown_edge in unknown_edges:
            node_source = str(unknown_edge[0])
            node_target = str(unknown_edge[1])
            if node_source in model.wv.vocab and node_target in model.wv.vocab:
                unknown_dist = model.wv.distance(*unknown_edge)
                errors = np.sum(negative_dists < unknown_dist)
                errors += 1 # For the ranking
                ranks.append(errors)
        
    
        rank = np.mean(ranks)
        
        numerator = np.arange(1, len(ranks) + 1)
        denominator = np.sort(ranks) + np.arange(len(ranks))
        MAP = (numerator / denominator).mean()
        
        return rank, MAP
    
    
'''
graph = karate_graph

fname_training = 'data/karate_training_01'
fname_testing = 'data/karate_testing_01'

unknown_graph = nx.read_edgelist(fname_testing)
unknown_edges = list(unknown_graph.edges)

known_graph = nx.read_edgelist(fname_training)
known_edges = list(known_graph.edges)
_, model = get_node2vec_embedding(known_graph)


# Edges of complete graph. 
# Undirected edges are assumed. 
complete_edges = []
nodes = list(graph.nodes)
for i, node_source in enumerate(nodes[:-1]):
    for node_target in nodes[i+1:]:
        node_source = str(node_source)
        node_target = str(node_target)
        if node_source in model.wv.vocab and node_target in model.wv.vocab:
            complete_edges.append((node_source, node_target))

        
# Edges to add to the original graph in order to obtain a complete graph
negative_edges = [negative_edge 
                  for negative_edge in complete_edges 
                  if (negative_edge not in known_edges 
                      and negative_edge not in unknown_edges)]
negative_dists = np.array([model.wv.distance(*negative_edge) for negative_edge in negative_edges])
ranks = []
for unknown_edge in unknown_edges:
    unknown_dist = model.wv.distance(*unknown_edge)
    errors = np.sum(negative_dists < unknown_dist)
    errors += 1 # For the ranking
    print(errors)
    ranks.append(errors)


rank = np.mean(ranks)

numerator = np.arange(1, len(ranks) + 1)
denominator = np.sort(ranks) + np.arange(len(ranks))
MAP = (numerator / denominator).mean()

print(rank, MAP)
'''