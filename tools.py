# -*- coding: utf-8 -*-
"""
Created on Wed Aug 29 10:18:16 2018

Module for basic operations.

@author: simon
"""
import numpy as np
import os
import pickle
import random

import constant


def write_txt(fname, string):
    """
    :param fname:
    :param string:
    """
    text_file = open(fname, "w")
    text_file.write(string)
    text_file.close()


def append_txt(fname, string):
    """
    :param fname:
    :param string:
    """
    text_file = open(fname, "a")
    text_file.write(string)
    text_file.close()


def all_files_in_data_dir(file_lst):
    """
    :param data_dir     Files that are assumed in the directory constant.data_dir
    :return:            True only if every file was found in the data directory
    """
    data_dir = constant.data_dir
    return np.all([os.path.exists(data_dir + f) for f in file_lst])


def save(filename, data, raw_filename=None):
    """
    Saves numpy matrix objects. No need to worry about the file ending.
    """
    if raw_filename is not None:
        pickle.dump(data, open(raw_filename + '.pkl', 'wb'))
    elif '.pkl' in filename:
        pickle.dump(data, open(constant.data_dir + filename, 'wb'))
    else:
        pickle.dump(data, open(constant.data_dir + filename + '.pkl', 'wb'))


def load(filename, raw_filename=None):
    """
    Loads numpy matrix objects. No need to worry about the file ending.
    """
    if raw_filename is not None:
        return pickle.load(open(raw_filename + '.pkl', 'rb'))
    else:
        return pickle.load(open(constant.data_dir + filename + '.pkl', 'rb'))
    

def split_array(arr, ratio, shuffeled=True):
    """
    Returns two subarrays where the first part is 
    of length (ratio * len(arr) )
    and the second of the length ((ratio - 1) * len(arr))
    """
    
    if shuffeled:
        random.shuffle(arr)
    
    a1_len = int(len(arr) * ratio)
    a1 = arr[:a1_len]
    a2 = arr[-(a1_len-1):]
    
    return a1, a2


def int2str(i, zfill=2):
    return ('%d'%i).zfill(zfill)