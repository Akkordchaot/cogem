# -*- coding: utf-8 -*-
"""
Created on 29.08.2018 at 08:35

@author: simon
"""

'''
Relative path variables
'''

# Zachary karate club data set (binary adjaceny matrix)
zachary_karate_club_filepath = './data/ucidata-zachary.dat'

# Blogcatalogue data set (binary adjacency matrix)
blogcatalog_filepath = './data/blogcatalog_network'

# Facebook edges from all egonets combined found in 
# https://snap.stanford.edu/data/ego-Facebook.html
fb_filepath = './data/facebook_combined.txt'

'''
Directories
'''
data_dir = 'data/'