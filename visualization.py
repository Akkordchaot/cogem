# -*- coding: utf-8 -*-
"""
Created on Wed Aug 29 20:05:34 2018

@author: simon
"""

import graph

import numpy as np
import gensim
import gensim.models.poincare
import gensim.viz.poincare as viz

import plotly.graph_objs as go

from collections import Counter

from matplotlib import pyplot as plt


def plot_poincare_model(model, pdf_fname='plotcircles2.pdf',
                        show_node_labels=True, num_nodes=None, 
                        fontsize = 7,
                        figsize = 12,
                        markersize = 5,
                        linewidth = 0.01):
    
    if num_nodes is None:
        num_nodes = model.kv.vectors.shape[0]
    
    # Visualization parameters of karate graph
    
    
    tree = [(str(i1), str(i2)) for (i1,i2) in model.all_relations]
    figure_title = "test"
    
    show_node_labels=[str(index) for index in list(model.indices_set)]
    vectors = model.kv.syn0
    if vectors.shape[1] != 2:
        raise ValueError('Can only plot 2-D vectors')
    
    node_labels = model.kv.index2word
    nodes_x = list(vectors[:, 0])
    nodes_y = list(vectors[:, 1])
    nodes = go.Scatter(
        x=nodes_x, y=nodes_y,
        mode='markers',
        marker=dict(color='rgb(30, 100, 200)'),
        text=node_labels,
        textposition='bottom'
    )
    
    nodes_x, nodes_y, node_labels = [], [], []
    for node in show_node_labels:
        vector = model.kv[node]
        nodes_x.append(vector[0])
        nodes_y.append(vector[1])
        node_labels.append(node)
    nodes_with_labels = go.Scatter(
        x=nodes_x, y=nodes_y,
        mode='markers+text',
        marker=dict(color='rgb(200, 100, 200)'),
        text=node_labels,
        textposition='bottom'
    )
    fig, ax = plt.subplots(figsize=(figsize,figsize))
    ax.set_xlim((-1.1, 1.1))
    ax.set_ylim((-1.1, 1.1))
    
    node_out_degrees = Counter(hypernym_pair[1] for hypernym_pair in tree)
        
    chosen_nodes = list(sorted(node_out_degrees.keys(), key=lambda k: -node_out_degrees[k]))[:num_nodes]
    
    for u, v in tree:
        if not(u in chosen_nodes or v in chosen_nodes):
            continue
        vector_u = model.kv[u]
        vector_v = model.kv[v]
        ax.plot([vector_u[0], vector_v[0]], [vector_u[1], vector_v[1]], 'c-', linewidth=linewidth)
    
    ax.plot(nodes['x'], nodes['y'], 'r.', markersize=markersize)
    poincare_border = plt.Circle((0,0), 1, color='k', fill=False, linewidth=1)
    
    if show_node_labels:
        for i in range(len(nodes_with_labels['text'])):
            plt.text(nodes_with_labels['x'][i], nodes_with_labels['y'][i], 
                     nodes_with_labels['text'][i], fontsize=fontsize)
    
    ax.add_artist(poincare_border)
    
    ax.axis('off')
    fig.savefig(pdf_fname, dpi=400)
    fig.show()
    
    

