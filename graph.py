# -*- coding: utf-8 -*-
"""
Created on Wed Aug 29 10:18:16 2018

@author: simon
"""

import os
import numpy as np
import pandas as pd
import networkx as nx
import gensim
import gensim.models.poincare


import constant
import tools


def get_adj_mat(filename):
    """
    From file to networkx (di)graph
    Load graph from csv (more precisely: whitespace-separated-file)

    :param filename:    Filename as string
    :return:            Returns a numpy adjacency matrix
    """
    # Load data set via pandas
    graph_df = pd.read_csv(filename, delim_whitespace=True, header=None)
    # Numpy adj-matrix representation
    graph_mat = graph_df.as_matrix()
    return graph_mat




def from_networkx_graph_to_igraph(networkx_graph):
    np_nodes = np.array(networkx_graph.nodes)
    np_edges = np.array(networkx_graph.edges)

    igraph = gensim.summarization.graph.Graph()

    for node_i in range(len(np_nodes)):
        igraph.add_node(str(np_nodes[node_i]))

    for edge_i in range(len(np_edges)):
        edge_tuple_as_str = (str(np_edges[edge_i][0]), str(np_edges[edge_i][1]))
        igraph.add_edge(edge_tuple_as_str)
    return igraph



def avg_degree(graph):
    """
    graph    A networkx graph
    """
    return np.array(graph.degree())[:,1].mean()

        
if __name__ == "__main__":
    
    data = load_data()
    karate_mat = data['karate_mat']
    karate_graph = data['karate_graph']
    catalog_mat = data['catalog_mat']
    catalog_graph = data['catalog_graph']
    fb_mat = data['fb_mat']
    fb_graph = data['fb_graph']
    
    #karate_poinc_emb = get_karate_poincare_emb()
    karate_avg_degree = avg_degree(karate_graph)
    
    fb_avg_degree = avg_degree(fb_graph)
    poincare_data = load_poincare_emb()
    
    karate_poinc = poincare_data['std_karate_poincare']
    fb_poinc = poincare_data['std_fb_poincare']
    